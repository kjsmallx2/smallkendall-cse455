//
//  ViewController.swift
//  CurrecyConverter
//
//  Created by Kendall Small on 1/24/18.
//  Copyright © 2018 Kendall Small. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var Label: UILabel!
    
    
    @IBOutlet weak var exchangeRateButton: UIButton!
    
    
    
    
    var url = "https://api.fixer.io/latest?base=USD"
    var rates = JSON()
    var currencyDropDown = DropDown()
    var currencyNames=[String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        currencyDropDown.anchorView = exchangeRateButton
        currencyDropDown.selectionAction = { [weak self] (index, currency) in self?.exchangeRateButton.setTitle(currency, for: .normal)
        }
        
        
        downLoadExchangeRates(_URL: url, success: { (ratesJSON) in
            self.rates = ratesJSON
            print(self.rates)
            for (key,_) in self.rates{
                self.currencyNames.append(key)
            }
            self.currencyDropDown.dataSource = self.currencyNames
            self.currencyDropDown.reloadAllComponents()
            
            
            
        }) { (error) in
            print(error)
        }
    }
    
    
    @IBAction func Show(_ sender: Any) {
        currencyDropDown.show()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Function for the button press. It should do the four different things that are within the function.
    @IBAction func Convert(_ sender: Any) {
        //Get input from the Text Field
        let textFieldValue=Double(textField.text!)
        
        //if statement to make sure user cannot leave this Text Field blank
        if textFieldValue != nil {
            let result = Double (textFieldValue! * rates[exchangeRateButton.currentTitle!].doubleValue)
            
            
            Label.text = "$\(textFieldValue!) = \(result)"
            //Clear text field after clicking the button
            textField.text=""
        } else {
            Label.text="This field cannot be blank!"
        }
    }
    
    func downLoadExchangeRates(_URL :String, success:@escaping (JSON)->Void, failure:@escaping (Error) -> Void){
        Alamofire.request(_URL).responseJSON { (response) -> Void in if response.result.isSuccess{
              //parse the json array into just the rates json object
              //Hint... Look at how the JSON object is given to you and what do we want from it?
            let responseJSON = JSON(response.result.value!)
            let ratesJSON = responseJSON["rates"]
            success(ratesJSON)
            }
            if response.result.isFailure {
                let error : Error = response.result.error!
                failure(error)
            }
            
        }
    }
    
}
