<?php
$host = 'localhost';
$username = 'root';
$password = '';
$dbname = 'cse455';

$dsn = 'mysql:host='.$host.';port=3306;dbname='. $dbname;

$name = $_GET['name'];
$amount = $_GET['rate'];
try {
  $pdo = new PDO($dsn, $username, $password);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE>EXCEPTION);
  
  $sql = 'SELECT * FROM Currency WHERE currency_name=?';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$name]);
  $posts = $stmt->fetch(PDO::FETCH_OBJ);
  
  $conObj->name = $posts->currency_name;
  $conObj->rate = $posts->currency_rate * $amount;
  echo json_encode($conObj);
}
catch(PDOException $e){
  echo "Connection failed: " . $e->getMessage();
}
?>